export interface IAuthor {
    id?: number,
    firstName : string,
    lastName : string,
    middleName : string
}