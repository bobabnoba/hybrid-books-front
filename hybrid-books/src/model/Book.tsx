import { IAuthor } from "./Author";

export interface IBook {
    id : number;
    title: string;
    authors : IAuthor[];
    description: string;
    imageUrl : string;
    isbn : string;
    quantity : number,
    creationDate: string
}