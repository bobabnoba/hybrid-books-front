import { IBook } from "./Book";

export interface Rent {
    id : number,
    book : IBook,
    returned : boolean
}