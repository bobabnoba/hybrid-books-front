export interface IJWTToken {
    username : string,
    expires : string
    role : string[]
}