import { useEffect, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { IBook } from '../../model/Book';
import { Rent } from '../../model/Rent';
import { getBooksPag, getBooksPagReset, getRented } from '../../redux/actions';
import { BooksState } from '../../redux/bookSlice';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import BookCard from '../BookCard/BookCard';
import './BookList.css';

const BookList = (props : {text:string}) => {

    const dispatch = useAppDispatch()
    const [page, setPage] = useState<number>(0)
    const {books} = useAppSelector<BooksState>(state => state.books);
    const {rents} = useAppSelector<BooksState>(state => state.books);

    useEffect(() => {
            dispatch(getBooksPagReset("0", "10", props.text)) 
    }, [props.text])

    useEffect(() => {
            dispatch(getRented())
    }, [rents])
    
    

    const fetchMoreData = () => {
        const currentPage = page + 1
        dispatch(getBooksPag(currentPage.toString(), "20", props.text))
        setPage(currentPage)
    }
    
    return(
            window.location.href.includes('rented') ?
            <div className='listing'>
                {rents?.map((r : Rent) => 
                <BookCard key={r.id} book={r.book}/>
            )}
            </div> 
            :
            <InfiniteScroll
                dataLength={books.length}
                next={fetchMoreData}
                hasMore={true}
                loader={<></>}
            >
                    <div className='listing'>

            {books.map((book : IBook, index) => 
                <BookCard key={index} book={book}/>
            )}
            </div>
            </InfiniteScroll>
    )
}
export default BookList

 