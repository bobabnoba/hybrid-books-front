import moment from 'moment';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { IAuthor } from '../../model/Author';
import { IBook } from '../../model/Book';
import { createBook, editBook, getAllAuthors, getBookById } from '../../services/service';
import './EditBook.css';

export default function EditBook() {

  const [title, setTitle] = useState<string>('')
  const [description, setDescription] = useState<string>('')
  const [published, setPublished] = useState<string>('')
  const [isbn, setIsbn] = useState<string>('')
  const [quantity, setQuantity] = useState<number>(0)
  const [authors, setAuthors] = useState<IAuthor[]>()
  const [imageUrl, setImageUrl] = useState<string>('')
  const [pageTitle, setPageTitle] = useState<string>('')
  const [selectedAuthors, setSelectedAuthors] = useState<IAuthor[]>()
  const { id } = useParams()
  const navigate = useNavigate()

  useEffect(() => {
    if(id){
      setPageTitle("Edit Book")
      getBookById(id).then(res =>
        {
          setTitle(res.data.title)
          setDescription(res.data.description)
          setPublished(res.data.creationDate)
          setIsbn(res.data.isbn)
          setQuantity(res.data.quantity)
          setSelectedAuthors(res.data.authors)
          setImageUrl(res.data.imageUrl)
          setSelectedAuthors(res.data.authors)
        })
        getAllAuthors().then( (res) => setAuthors(res.data))
    } else {
      setPageTitle("Create New Book")
      getAllAuthors().then( (res) => setAuthors(res.data))
      setSelectedAuthors([])
    }
  }, [])

  const authorSelected = (event : any) => {
    const id = event.target.value
    const exists = selectedAuthors?.some(a => a.id == id)
    if(!exists){
      authors?.forEach((a) => {
        if (a.id == id )
        selectedAuthors?.push(a)
      })
    } else {
        selectedAuthors?.splice(selectedAuthors.findIndex(aut => aut.id == id), 1);
    } 
  }

  const handleSubmit = (event : any) => {

    event.preventDefault();
    if(id) {
    const updatedBook : IBook = {
      id : Number(id),
      title : title,
      description : description,
      creationDate : published,
      isbn : isbn,
      quantity : quantity,
      authors : selectedAuthors as IAuthor[],
      imageUrl : imageUrl
      }
      console.log(updatedBook)
      editBook(updatedBook.id, updatedBook).then( () => navigate('/books') )
    } else {
      createBook({
        title,
        description, 
        creationDate : moment(new Date()).format('YYYY-MM-DD').toString(),
        isbn,
        quantity,
        authors : selectedAuthors as IAuthor[],
        imageUrl
      } as IBook).then( () => {alert('New book added!'), navigate('/books')})
    }
  
  };
    
  return (
    <div className="edit-container">
      <div className="edit-content">
        <h1>{pageTitle}</h1>
        <form onSubmit={handleSubmit}>
        
          <fieldset className="edit-fieldset">
            <div className="grid-35">
              <label>Title</label>
            </div>
            <div className="grid-65">
              <input value = {title} onChange={(e) => setTitle(e.target.value)} className= "edit-input" type="text"/>
            </div>
          </fieldset>
          <fieldset className="edit-fieldset">
            <div className="grid-35">
              <label >Author</label>
            </div>
            <div className="grid-65">
            
            <select multiple={true} onChange={ authorSelected } className="edit-select">
              {authors?.map((item, id) => (<option key={id} value={item.id} selected={selectedAuthors?.some(a => a.id == item.id)}><span key={id}>{item.firstName} {item.lastName}  </span></option>))}
            </select>
            </div>
          </fieldset>
          <fieldset className="edit-fieldset">
            <div className="grid-35">
              <label>Description</label>
            </div>
            <div className="grid-65">
              <input value = {description} onChange={(e) => setDescription(e.target.value)} className= "edit-input" type="text"/>
            </div>
          </fieldset>
          <fieldset className="edit-fieldset">
            <div className="grid-35">
              <label>ISBN</label>
            </div>
            <div className="grid-65">
              <input value ={ isbn} onChange={(e) => setIsbn(e.target.value)} className= "edit-input" type="text"/>
            </div>
        </fieldset>
        <fieldset className="edit-fieldset">
            <div className="grid-35">
              <label>Quantity</label>
            </div>
            <div className="grid-65">
              <input value={quantity} onChange={(e) => setQuantity(e.target.valueAsNumber)} className= "edit-input" type="number"/>
            </div>
        </fieldset>
        <fieldset className="edit-fieldset">
            <div className="grid-35">
              <label>Image URL</label>
            </div>
            <div className="grid-65">
              <input value={imageUrl} onChange={(e) => setImageUrl(e.target.value)} className= "edit-input" type="text"/>
            </div>
        </fieldset>
        <hr />
        <button className='edit-button'>Save</button>
        </form>
      </div>
    </div>
  )
}
