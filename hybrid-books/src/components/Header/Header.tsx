/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { removeAllRoles } from '../../redux/actions';
import { useAppDispatch } from '../../redux/hooks';
import { logout } from '../../services/service';
import "./Header.css";


const Header = (props : {search: (text: string) => void}) => {
    
    const [searchIcon, setSearch] = useState("no-search fa fa-search")
    const [searchInput, setSearchInput] = useState("search-input")
	const [searchText, setSearchText] = useState('')
	const dispatch = useAppDispatch()
	const navigate = useNavigate();
	const isAdmin = localStorage.getItem('roles') ? localStorage.getItem('roles')!.includes('ROLE_ADMIN') : false;
	const loggedInExists = localStorage.getItem('token') != null
	

	const handleSearch = (event : any) => {
		setSearchText(event.target.value);
		props.search(event.target.value)
	}
	const  changeSearchClass = () => {
		setSearch("search fa fa-search") 
		setSearchInput("search-active")
	}

	const navigateTo = () => {
		if (isAdmin) navigate('/create')
		else navigate('/rented')
	}

	const logMeOut = () => {
		logout()
		dispatch(removeAllRoles())
		navigate("/login")
	}
  return (
<header className="header">
	<nav className="navbar container">
		<a href="/books" className="brand"> <img className="logo-img" src='https://seeklogo.com/images/L/library-books-logo-4189717FC3-seeklogo.com.jpg'></img> Hybrid Books</a>
		<div className="menu" id="menu">
			<ul className="menu-list">
				<li className="menu-item">
					<a href="/books" className="menu-link is-active">
						<i className="menu-icon"></i>
						<span className="menu-name">{loggedInExists ? "Home" : ""}</span>
					</a>
				</li>
				<li className="menu-item">
					<a  onClick={navigateTo}  className="menu-link is-active">
						<i className="menu-icon"></i>
						<span className="menu-name">{ loggedInExists ? ( isAdmin ? "Add book" : "Rented") : ""}</span>
					</a>
				</li>
				<li className="menu-item">
					<a onClick= {logMeOut} className="menu-link">
						<i className="menu-icon" ></i>
						<span className="menu-name"> { loggedInExists ? "Logout" : "Login"} </span>
					</a>
				</li>
                <li className="menu-item">
					{ loggedInExists ? 	<i className={searchIcon}  onClick={changeSearchClass}></i> : <></>}
                        <input className={searchInput} onChange={handleSearch}  value={searchText} type="text" placeholder="Search.."></input>
				</li>
			</ul>
		</div>
	</nav>
</header>
  )
} 
export default Header
