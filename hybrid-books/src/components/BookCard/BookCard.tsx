import { useEffect, useState } from 'react';
import { IBook } from '../../model/Book';
import { deleteBookWithId } from '../../redux/actions';
import { useAppDispatch } from '../../redux/hooks';
import { rentABook } from '../../services/service';
import './BookCard.css';


const BookCard = (props : {book:IBook}) => {

    const [book, setBook] = useState(props.book);
    const dispatch = useAppDispatch()

    useEffect(() => {
        setBook(props.book)
    }, [props])
    
    const deleteBook = () => {
      dispatch(deleteBookWithId(book.id))
      }

      const rentBook = () => {
        rentABook(book.id).then(
          () => alert("Book rented successfuly!"),
          (err) => alert("Could not rent this book. " + err.response.data)
        )
      }

    return (
            <div className="book-item">
              <div className="bk-img">
                <div className="bk-wrapper">
                  <div className="bk-book bk-bookdefault">
                    <div className="bk-front">
                      <div className="bk-cover">
                        <img className="cover-img" src={book.imageUrl} alt="" />
                      </div>
                    </div>
                    <div className="bk-back"></div>
                    <div className="bk-left"></div>
                  </div>
                </div>
              </div>
              <div className="item-details">
                <h3 className="book-item_title">{book.title}</h3>
                <p className="author">by {book.authors?.map((item, id) => (<span key={id}> {item.firstName} {item.lastName}  </span>))}
                
                
                &bull; {book.quantity} available copies </p>
                <p className='description'>{book.description}
                 </p>

                { localStorage.getItem('roles')!.includes('ROLE_ADMIN') ? 
              
                  <span><a href={'edit/' + book.id} className="button">Edit</a>
                  <a onClick={deleteBook} className="button">Delete</a></span>
                

                :  <a onClick={rentBook} className="button">Rent</a> }
                
               
              </div>
            </div>
 
    )
}
export default BookCard