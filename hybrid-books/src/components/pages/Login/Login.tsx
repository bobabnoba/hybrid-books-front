import jwt from 'jwt-decode';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { IJWTToken } from '../../../model/JWTToken';
import { setAllRoles } from '../../../redux/actions';
import { useAppDispatch } from '../../../redux/hooks';
import { login } from '../../../services/service';
import './Login.css';

export default function Login(){

  const [username, setUsername] = useState<string>('')
  const [password, setPassword] = useState<string>('')
  const dispatch = useAppDispatch()
  const navigate = useNavigate();


  const handleSubmit = (event : any) => {

    event.preventDefault();
    
    login(username, password)
    .then(res => {
          const decoded  : IJWTToken = jwt<IJWTToken>(res.data.token)
          dispatch(setAllRoles(decoded.role))
          localStorage.setItem('roles', JSON.stringify(decoded.role))
          localStorage.setItem("token", res.data.token)
          navigate("/books")
  })
  };

    return (
        <div className="login-container">
        <div className="login-content">
          <h1>Welcome back!</h1>
          <form onSubmit={handleSubmit}>
          
            <fieldset className="login-fieldset">
              <div className="grid-35">
                <label>Username</label>
              </div>
              <div className="grid-65">
                <input name="username" value = {username} onChange={(event)=>setUsername(event.target.value)} className= "login-input" type="text"/>
              </div>
            </fieldset>
            <fieldset  className="login-fieldset">
              <div className="grid-35">
                <label>Password</label>
              </div>
              <div className="grid-65">
                <input name = "password" value = {password} onChange={(event)=>setPassword(event.target.value)} className= "login-input" type="password"/>
              </div> 
            </fieldset>
          <hr />
          <button  type="submit" className='login-button'>Login</button>
          </form>
        </div>
      </div>
    )
}