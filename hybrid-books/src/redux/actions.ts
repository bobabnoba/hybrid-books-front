/* eslint-disable no-unused-vars */
import { deleteBook, getAllBooks, getAllBooksWithPagination, getBookById } from "../services/service"
import { bookByIdFetched, bookDeleted, bookFetchedPag, booksFetched, booksFetchedPagReset, rentedBooksFetched } from "./bookSlice"
import { removeRoles, setRoles } from "./loginSlice"

export const getBooks = () => async (dispatch: (arg: any) => any) => {
      const response = await getAllBooks()
      dispatch(booksFetched(response.data))
  }

  export const setAllRoles = (payload : string[]) => async (dispatch: (arg: any) => any) => {
    dispatch(setRoles(payload))
}

export const removeAllRoles = () => async (dispatch: (arg: any) => any) =>  {
  dispatch(removeRoles())
}

export const getBook = (id : number)  => async (dispatch: (arg: any) => any) =>  {
  const response = await getBookById(id)
  dispatch(bookByIdFetched(response.data))
}

export const getBooksPag = (currPage : string, pageSize : string, input : string) => async (dispatch: (arg: any) => any) => {
  const response = await getAllBooksWithPagination(currPage, pageSize, input)
  dispatch(bookFetchedPag(response.data))
}

export const getBooksPagReset = (currPage : string, pageSize : string, input : string) => async (dispatch: (arg: any) => any) => {
  const response = await getAllBooksWithPagination(currPage, pageSize, input)
  dispatch(booksFetchedPagReset(response.data))
}

export const deleteBookWithId = (id : number) => async (dispatch: (arg: any) => any) => {
  deleteBook(id)
  dispatch(bookDeleted({id}))
}

export const getRented = () => async (dispatch: (arg: any) => any) => {
  const response = await getAllBooks()
  dispatch(rentedBooksFetched(response.data))
}