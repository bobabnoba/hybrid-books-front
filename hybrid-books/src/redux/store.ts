import { configureStore } from '@reduxjs/toolkit'
import bookSlice from './bookSlice'
import loginSlice from './loginSlice'


 const store = configureStore({
  reducer: {
      books : bookSlice,
      login : loginSlice
  }
}) 
export default store

export type RootState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch