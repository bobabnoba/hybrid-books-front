import { createSlice } from "@reduxjs/toolkit"

export interface LoginState {
    roles : string[]
}

const initialState : LoginState = {
    roles : []
}

export const loginSlice = createSlice({
    name : 'login',
    initialState,
    reducers: {
        setRoles(state, action) {
            state.roles = action.payload
        },
        removeRoles(state) {
            state.roles = []
        }
    }
})


export const { setRoles, removeRoles } = loginSlice.actions

export default loginSlice.reducer
