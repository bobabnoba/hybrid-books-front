import { createSlice } from '@reduxjs/toolkit'
import { IBook } from '../model/Book'
import { Rent } from '../model/Rent'

export interface BooksState {
  books : IBook[],
  book : IBook,
  rents : Rent[]
}

const initialState: BooksState = {
  books : [],
  book : {} as IBook,
  rents : []
}

export const bookSlice = createSlice({
  name: 'books',
  initialState,
  reducers: {
    booksFetched(state, action) {
        state.books = action.payload
    },
    bookByIdFetched(state, action){
      state.book = action.payload
    }, bookFetchedPag(state, action){
      state.books = [...state.books, ...action.payload ]
    }, 
    bookDeleted(state, action){ 
      state.books = state.books.filter(({ id }) => id !== action.payload.id)
    },
    rentedBooksFetched(state, action) {
      state.rents = action.payload
    },
    booksFetchedPagReset(state, action) {
      state.books = action.payload
    }
    
 
  }
})

 export const { booksFetched, bookByIdFetched, bookFetchedPag, bookDeleted, rentedBooksFetched, booksFetchedPagReset } = bookSlice.actions

 export default bookSlice.reducer
