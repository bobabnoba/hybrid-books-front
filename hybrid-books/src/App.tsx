import React, { useEffect, useState } from 'react';
import { Route, Routes, useNavigate } from 'react-router-dom';
import './App.css';
import BookList from './components/BookList/BookList';
import EditBook from './components/EditBook/EditBook';
import Header from './components/Header/Header';
import Login from './components/pages/Login/Login';


function App () {
  let navigate = useNavigate();
  const [searchText, setSearchText] = useState('')
  useEffect(() => {
    if(!localStorage.getItem('token')){
     navigate("/login")
    }
  }, []);
  
  const hSearch = (text : string) => {
    setSearchText(text)
  }

  const roles = localStorage.getItem('roles') ?? ''
  const isAdmin = roles.includes('ADMIN')
  const isUser = roles.includes('USER')

  return (
     <div>
        <Header search={hSearch} />
    <div className='books'>
        <Routes>
          { isAdmin || isUser ? <Route path="/books" element={ <BookList text={searchText}/> }/> : <></> }
          <Route path="/login" element={ <Login/> }/>
          { isAdmin ?  <Route path="/edit/:id" element={ <EditBook/> }/> : <></>} 
          { isAdmin ?  <Route path="/create" element={ <EditBook/> }/> : <></>}
          { isUser ?  <Route path = "/rented" element = {< BookList text={''}/> }/> : <></>}

        </Routes>
      
    </div>
     </div>
  )
}

export default App 
