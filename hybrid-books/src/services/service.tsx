import axios from 'axios';
import { IAuthor } from '../model/Author';
import { IBook } from '../model/Book';

export const getAllBooks = () => {
    return axios.get("http://54.93.246.187:8080/api/v3/rent/personal", {headers: { Authorization : 'Bearer ' + authToken()}});
}

export const rentABook = (bookId : number) => {
    return axios.post("http://54.93.246.187:8080/api/v3/rent/", {bookId}, {headers: { Authorization : 'Bearer ' + authToken()}});
}

export const getAllBooksWithPagination = (page: string, pageSize: string, input: string) => {
    const query = `?page=${page}&pageSize=${pageSize}&title=${input}`
    return axios.get("http://54.93.246.187:8080/api/v3/books" + `${query}`)
}

export const getBookById = (id : any) => {
    return axios.get("http://54.93.246.187:8080/api/v3/books/" + id)
}

export const editBook = (id : number, updatedBook : IBook) => {
    return axios.put("http://54.93.246.187:8080/api/v3/books/" + id, updatedBook, {headers: { Authorization : 'Bearer ' + authToken()}});
}

export const createBook = (newBook : IBook) => {
    return axios.post("http://54.93.246.187:8080/api/v3/books/", newBook, {headers: { Authorization : 'Bearer ' + authToken()}});
}

export const deleteBook = (id : number) => {
    return axios.delete("http://54.93.246.187:8080/api/v3/books/" + id, {headers: { Authorization : 'Bearer ' + authToken()}})
    .then( () => alert("Successfuly deleted."),
           (err) => alert("Book coluld not be deleted. " + err.response.data)
    );
}

export const getAllAuthors = () => {
    return axios.get("http://54.93.246.187:8080/api/v3/authors", {headers: { Authorization : 'Bearer ' + authToken()}});
}

export const addNewAuthor = (newAuthor : IAuthor) => {
    return axios.post("http://54.93.246.187:8080/api/v3/authors", newAuthor, {headers: { Authorization : 'Bearer ' + authToken()}});

}

export const login = (username : string, password : string ) => {
    return axios.post("http://54.93.246.187:8080/api/v3/auth/login", {
        username,
        password
    })
}

export const logout = () => {
    localStorage.removeItem('token')
}


export const authToken = () => {
    const token = localStorage.getItem('token')
    if (token) {
        return token
    }
    return ''

}